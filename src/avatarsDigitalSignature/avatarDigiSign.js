import React, { useCallback, useState, useRef, useMemo } from "react";
import {
  AVAILABLE_FONT_NAMES,
  convertArrayBufferToB64,
  handleCopy,
  handleDownload,
  queryParams,
} from "./fontNames";
import axios from "axios";
import "./index.css";

const avatarBaseUrl = "https://apihut.in/api/avatar";

export default function DigitalSignature() {
  const [signature, setSignature] = useState("");
  const [currentBatch, setCurrentBatch] = useState(0);
  const [signatureImg, setSignatureImg] = useState([]);
  const [loader, setLoader] = useState(false);
  const containerRef = useRef(null);

  const handleSignature = useCallback((e) => {
    setSignature(e.target?.value);
    setSignatureImg({});
  }, []);

  const createDigitalSignature = useCallback(
    async (batch) => {
      setLoader(true);
      const apiUrl = `${avatarBaseUrl}/${signature}`;
      try {
        let currBatch = batch ?? currentBatch;
        const batchEndLimit = currBatch + 6;
        const fontBatchList = AVAILABLE_FONT_NAMES.slice(
          currBatch,
          batchEndLimit
        );
        const fontPromises = fontBatchList?.map(async (font) => {
          const response = await axios.get(apiUrl, {
            responseType: "arraybuffer",
            params: { ...queryParams, fontPath: font },
            headers: {
              // Add your headers here
              "X-Avatar-Key": "Bearer <YOUR_ACCESS_TOKEN>",
            },
          });
          return response.data;
        });

        let results = await Promise.all(fontPromises);
        // convert result to base64 image
        results = convertArrayBufferToB64(results);
        results = fontBatchList.reduce(function (obj, currentValue, index) {
          obj[currentValue] = results[index];
          return obj;
        }, {});

        // Handle the results of all API requests
        setSignatureImg((prev) => ({ ...prev, ...results }));
        // setFontName((prev) => [...prev, ...fontBatchList]);
        setCurrentBatch(batchEndLimit);
      } catch (error) {
        // Handle errors during the requests
        console.error("Axios error:", error);
      } finally {
        setLoader(false);
      }
    },
    [signature, currentBatch]
  );

  const loadMoreSign = useCallback(() => {
    const container = containerRef.current;
    if (
      container.scrollTop + container.clientHeight >=
      container.scrollHeight
    ) {
      createDigitalSignature();
    }
  }, [createDigitalSignature]);

  const signatureKeys = useMemo(() => {
    return Object.keys(signatureImg);
  }, [signatureImg]);

  const signatureValues = useMemo(() => {
    return Object.values(signatureImg);
  }, [signatureImg]);

  return (
    <div className="container">
      <h5 className="text-center my-3">Digital Signature Application</h5>
      <div className="input-group py-5">
        <input
          type="search"
          className="form-control"
          placeholder="Enter your name for a digital signature."
          onInput={handleSignature}
          style={{
            border: "0px",
            borderBottom: "1px solid black",
          }}
        />
        <div className="input-group-append">
          <button
            className="btn btn-dark"
            type="button"
            onClick={() => {
              createDigitalSignature(0);
            }}
          >
            <i className="fa fa-search"></i>
          </button>
        </div>
      </div>
      <div>
        <div
          className="row m-0 p-5 mt-3 mb-5 shadow rounded"
          ref={containerRef}
          onScroll={loadMoreSign}
          style={{
            height: "290px",
            maxHeight: "290px",
            overflowY: "auto",
            border: "1px solid grey",
          }}
        >
          {signatureKeys?.length ? (
            signatureValues.map((sign, index) => (
              <div
                className="col-md-4 mb-5 sign-container"
                key={index}
                style={{ position: "relative", overflow: "hidden" }}
              >
                <DownloadOptions signature={sign} />
                <div
                  className="text-center rounded p-3 "
                  style={{ border: "1px dashed magenta", cursor: "pointer" }}
                >
                  <img
                    className="img-fluid" // Use img-fluid class for responsive images
                    src={sign}
                    alt={`Card ${index + 1}`}
                  />
                </div>
                <p className="text-center text-secondary">
                  <b>
                    <i>{signatureKeys[index]}</i>
                  </b>
                </p>
              </div>
            ))
          ) : (
            <h5 className="text-center w-100">
              Generate a digital signature by entering your name
            </h5>
          )}
          {loader && (
            <div className="spinner-border m-auto" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

const DownloadOptions = React.memo(({ signature }) => {
  return (
    <div
      className="px-1 icon-hidden"
      style={{
        position: "absolute",
        height: "72px",
        right: "-20px",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
      }}
    >
      <i
        className="fas fa-copy "
        data-signature={signature}
        onClick={handleCopy}
        style={{ cursor: "pointer", color: "violet" }}
        data-toggle="tooltip"
        title="Copy base64 image data"
      ></i>
      <i
        className="fas fa-download "
        data-signature={signature}
        onClick={handleDownload}
        style={{ cursor: "pointer", color: "violet" }}
        data-toggle="tooltip"
        title="Download image"
      ></i>
    </div>
  );
});
