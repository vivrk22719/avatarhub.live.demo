const AVAILABLE_FONT_NAMES = [
  "Agbalumo Regular",
  "Alex Brush Regular",
  "Allura Regular",
  "Amatic SC Bold",
  "Amatic SC Regular",
  "Architects Daughter Regular",
  "Arizonia Regular",
  "Baloo 2",
  "Bungee Spice Regular",
  "Cabin Sketch Bold",
  "Cabin Sketch Regular",
  "Calligraffitti Regular",
  "Caveat",
  "Courgette Regular",
  "Dancing Script",
  "Dawning of a New Day Regular",
  "Dot Gothic 16 Regular",
  "Ephesis Regular",
  "Freehand Regular",
  "Fuggles Regular",
  "Genos Italic",
  "Genos",
  "Gloria Hallelujah Regular",
  "Great Vibes Regular",
  "Herr Von Muellerhoff Regular",
  "Homemade Apple Regular",
  "Indie Flower Regular",
  "Julee Regular",
  "Kaushan Script Regular",
  "Kristi Regular",
  "Long Cang Regular",
  "Marck Script Regular",
  "Monsieur La Doulaise Regular",
  "Mr Dafoe Regular",
  "Nothing You Could Do Regular",
  "Pacifico Regular",
  "Parisienne Regular",
  "Pinyon Script Regular",
  "Quicksand",
  "Reenie Beanie Regular",
  "Roboto Condensed Italic",
  "Roboto Condensed",
  "Rouge Script Regular",
  "Rubik Doodle Shadow Regular",
  "Rubik Doodle Triangles Regular",
  "Rubik Glitch Regular",
  "Rubik Maps Regular",
  "Rubik Scribble Regular",
  "Sacramento Regular",
  "Satisfy Regular",
  "Seaweed Script Regular",
  "Sevillana Regular",
  "Shadows Into Light Regular",
  "Sofia Regular",
  "Space Mono Bold",
  "Space Mono Bold Italic",
  "Space Mono Italic",
  "Space Mono Regular",
  "Tangerine Bold",
  "Tangerine Regular",
  "Updock Regular",
  "Whisper Regular",
  "Wind Song Medium",
  "Wind Song Regular",
  "Yellowtail Regular",
  "Zeyada Regular",
];
let queryParams = {
  noSlice: true,
  bgColor: "transparent",
  textColor: "magenta",
  fontSize: 21,
  width: 200,
};

function convertArrayBufferToB64(results) {
  // convert result to base64 image
  return results.map((result) => {
    return `data:image/png;base64,${btoa(
      new Uint8Array(result).reduce(
        (data, byte) => data + String.fromCharCode(byte),
        ""
      )
    )}`;
  });
}
const getSignature = (e) => e.target.getAttribute("data-signature");

const handleDownload = (e) => {
  const signature = getSignature(e);
  // converting base64 data to blob after parsing it
  const base64String = signature.replace(
    /^data:image\/(png|jpg|jpeg);base64,/,
    ""
  );
  const byteArray = new Uint8Array(
    [...atob(base64String)].map((char) => char.charCodeAt(0))
  );
  const blob = new Blob([byteArray], { type: "image/png" });

  const link = document.createElement("a");
  link.href = URL.createObjectURL(blob);
  link.download = "signature.png";

  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

const handleCopy = async (e) => {
  const signature = getSignature(e);
  await navigator.clipboard.writeText(signature);
  alert(`Base64 image copied successfully`);
};

module.exports = {
  AVAILABLE_FONT_NAMES,
  queryParams,
  convertArrayBufferToB64,
  handleDownload,
  handleCopy,
};
